#!/usr/bin/env bash
{ # this ensures the entire script is downloaded #
set -eEuo pipefail
trap 'trap_parser $?' ERR

declare -a DEPS
DEPS=(docker jq bc)

NETWORK_DIR="network"
ENV_PROPS=".env"

PROJECT_DIR="$(cd "$(dirname "$0")" && pwd)"
NETWORKS=",$(find "${PROJECT_DIR}/${NETWORK_DIR}" -type f -exec sh -c 'filename=$(basename -- "$1") && printf "${filename%.*},"' _ {} \;)"

main() {
  cd "${PROJECT_DIR}"
  deps "${DEPS[@]}"
  parse_args "$@"
}

deps() {
  for dep in "$@"; do
  if ! command -v "${dep}" > /dev/null 2>&1; then
    console_log "ERR ${dep} command not found in path"
    exit 1
  fi
  done
}

usage() {
cat << EOF
usage: ${0##*/} subcommand [options] [args]

subcommands

  set      - set the current validator network
  build    - builds the container
  run      - run commands from within the container
  start    - starts the container
  logs     - tails the container's logs
  exec     - run commands inside the running container
  status   - get container status
  identity - shows/sets priv_validator_key.json from identity/*.json
    list   - shows identity/*.json
    save   - saves priv_validator_key.json to identity/*.json
    remove - removes identity from identity/*.json
    rename - rename identity from identity/*.json
    scp    - scp identity to a remote machine via ssh
  rsync    - rsync the validator data to a remote machine via ssh 
  stop     - stops the container
  remove   - removes the container
  restart  - restarts the container
  update   - updates the running container
  clean    - clean unused docker images and volumes
  purge    - stop all running containers, purge everything
EOF
}

parse_args(){
  if [[ $# -lt 1 ]];then usage;exit 1; fi
  subc=$1
  case $subc in
    set)      shift; if [[ $# -ne 1 ]]; then usage; exit 1; fi; setenv "$1" ;;
    build)    getenv; docker_build ;;
    run)      shift; if [[ $# -lt 1 ]]; then usage; exit 1; fi; getenv; docker_run "$@" ;;
    start)    getenv; docker_start ;;
    logs)     getenv; docker_logs ;;
    exec)     shift; if [[ $# -lt 1 ]]; then usage; exit 1; fi; getenv; docker_exec "$@" ;;
    status)   getenv; get_status ;;
    identity) shift; getenv; priv_json_switch "$@" ;;
    rsync)    shift; if [[ $# -ne 1 ]]; then usage; exit 1; fi; getenv; rsync_data "$1" ;;
    stop)     getenv; docker_stop ;;
    remove)   getenv; docker_remove;;
    restart)  getenv; docker_stop; docker_start ;;
    update)   getenv; docker_build; docker_remove; docker_start ;;
    clean)    docker system prune -a --volumes -f ;;
    purge)    docker stop "$(docker ps -qa)" || true; docker system prune -a --volumes -f ;;
    *)        usage; exit 1 ;;
  esac
}

docker_build() {
  # shellcheck source=/dev/null
  source "${PROJECT_DIR}/${NETWORK_DIR}/${NETWORK}.env"
  trace "docker build '${DOCKERFILE_DIR}' -t '${DOCKER_TAG}' \
    --build-arg TENDERMINT_USER='${TENDERMINT_USER}' \
    --build-arg GO_VERSION='${GO_VERSION}' \
    --build-arg REPO='${REPO}' \
    --build-arg CHECKOUT='${CHECKOUT}' \
    --build-arg MAKE_TARGETS='${MAKE_TARGETS:-install}' \
    --build-arg SERVER_BIN='${SERVER_BIN}' \
    --build-arg CLI_BIN='${CLI_BIN}' \
    --build-arg START_CMD='${START_CMD}' \
    --build-arg SEEDS='${SEEDS}' \
    --build-arg PEERS='${PEERS}' \
    --build-arg RPC_LADDR='${RPC_LADDR}'"
}

docker_run() {
  # shellcheck source=/dev/null
  source "${PROJECT_DIR}/${NETWORK_DIR}/${NETWORK}.env"
  trace "docker run --rm -it \
    -v '${DATA_DIR}/server:/home/${TENDERMINT_USER}/.${SERVER_BIN}' \
    -v '${DATA_DIR}/cli:/home/${TENDERMINT_USER}/.${CLI_BIN}' \
    ${DOCKER_TAG} $*"
}

docker_start() {
  # shellcheck source=/dev/null
  source "${PROJECT_DIR}/${NETWORK_DIR}/${NETWORK}.env"
  docker_ps="$(docker ps -a -f name="${DOCKER_TAG}" | grep -c "${DOCKER_TAG}" || true)"
  if [[ ${docker_ps} -eq 1 ]]; then
     trace "docker start ${DOCKER_TAG}"
  elif [[ ${docker_ps} -eq 0 ]]; then
    trace "docker run -d \
      --restart=unless-stopped \
      --name ${DOCKER_TAG} \
      -p '${HOST_PORT_P2P}:26656' \
      -p '${HOST_PORT_RPC}:26657' \
      -v '${DATA_DIR}/server:/home/${TENDERMINT_USER}/.${SERVER_BIN}' \
      -v '${DATA_DIR}/cli:/home/${TENDERMINT_USER}/.${CLI_BIN}' \
      ${DOCKER_TAG}"
  else
    echo "ERR found more than one container with the same name, this shouldn't happen"
    exit 1
  fi
  while [[ ${hc_status:-null} != healthy ]]; do
    sleep 5 & wait
    hc_status="$(docker inspect "${DOCKER_TAG}" | jq -j '.[].State.Health.Status')"
    console_log "INFO ${DOCKER_TAG} healthcheck:${hc_status}"
  done
  get_status
}

docker_exec() {
  # shellcheck source=/dev/null
  source "${PROJECT_DIR}/${NETWORK_DIR}/${NETWORK}.env"
  if [[ ${1} == root ]]; then
    GOSU_USER="root"
    shift
  else
    GOSU_USER="${TENDERMINT_USER}"
  fi
  docker_ps="$(docker ps -f name="${DOCKER_TAG}" | grep -c "${DOCKER_TAG}" || true)"
  if [[ ${docker_ps} -eq 1 ]]; then
    trace "docker exec -it ${DOCKER_TAG} gosu ${GOSU_USER} $*"
  elif [[ ${docker_ps} -eq 0 ]]; then
    echo "ERR container ${DOCKER_TAG} in not currently running"
    exit 1
  else
    echo "ERR found more than one container with the same name, this shouldn't happen"
    exit 1
  fi
}

docker_stop() {
  # shellcheck source=/dev/null
  source "${PROJECT_DIR}/${NETWORK_DIR}/${NETWORK}.env"
  docker_ps="$(docker ps -f name="${DOCKER_TAG}" | grep -c "${DOCKER_TAG}" || true)"
  if [[ ${docker_ps} -eq 1 ]]; then
    trace "docker stop ${DOCKER_TAG}"
  elif [[ ${docker_ps} -eq 0 ]]; then
    :
  else
    echo "ERR found more than one container with the same name, this shouldn't happen"
    exit 1
  fi
}

docker_remove() {
  # shellcheck source=/dev/null
  source "${PROJECT_DIR}/${NETWORK_DIR}/${NETWORK}.env"
  docker_ps="$(docker ps -a -f name="${DOCKER_TAG}" | grep -c "${DOCKER_TAG}" || true)"
  if [[ ${docker_ps} -eq 1 ]]; then
    trace "docker stop ${DOCKER_TAG}"
    trace "docker rm ${DOCKER_TAG}"
  elif [[ ${docker_ps} -eq 0 ]]; then
    :
  else
    echo "ERR found more than one container with the same name, this shouldn't happen"
    exit 1
  fi
}

docker_logs() {
  # shellcheck source=/dev/null
  source "${PROJECT_DIR}/${NETWORK_DIR}/${NETWORK}.env"
  trace "docker logs -f --tail=10 ${DOCKER_TAG}"
}

priv_json_vars() {
  priv_json_destination="${DATA_DIR}/server/config/${PRIV_JSON}"
  if test -f "${priv_json_destination}"; then
    priv_json_active="${priv_json_destination}"
    while IFS= read -r -d '' file; do
      if diff -q "${file}" "${priv_json_destination}" >/dev/null 2>&1; then
        priv_json_active="${file}"
        break
      fi
    done < <(find "${DATA_DIR}/identity" -type f -name '*.json' -print0)
  else priv_json_active="none"; fi
}

get_status() {
  # shellcheck source=/dev/null
  source "${PROJECT_DIR}/${NETWORK_DIR}/${NETWORK}.env"
  status="$(docker inspect "${DOCKER_TAG}")"
  healthcheck="$(echo "${status}" | jq -j '.[].State.Health.Status')"
  if [[ ${healthcheck} == healthy ]]; then
    status_json="$(docker exec -it "${DOCKER_TAG}" curl -sf http://localhost:26657/status)"
    voting_power="$(echo "${status_json}" | jq -j .result.validator_info.voting_power)"
    catchup="$(echo "${status_json}" | jq -j .result.sync_info.catching_up)"
    latest_block_height="$(echo "${status_json}" | jq -j .result.sync_info.latest_block_height)"
    latest_block_timestamp="$(echo "${status_json}" | jq -j .result.sync_info.latest_block_time)"
    now_epoch="$(date +%s)"
    latest_block_epoch="$(date -d "${latest_block_timestamp}" +%s)"
    seconds_ago="$(echo "${now_epoch} - ${latest_block_epoch}" | bc)"
    moniker="$(echo "${status_json}" | jq -j .result.node_info.moniker)"
    keys_json="$(docker exec -it "${DOCKER_TAG}" gosu "${TENDERMINT_USER}" timeout 2.5 "${CLI_BIN}" keys list -o json || true)"
    echo "${keys_json}" | jq -e '.' >/dev/null 2>&1 || keys_json='[{"address": "error - passphrase protected chain most likely"}]'
    validators_json="$(docker exec -it "${DOCKER_TAG}" "${CLI_BIN}" query staking validators --chain-id "${CHAIN_ID}" -o json || true)"
    echo "${validators_json}" | jq -e '.' >/dev/null 2>&1 || {
      validators_json="$(docker exec -it "${DOCKER_TAG}" "${CLI_BIN}" stake validators -o json)"
    }
    validator_json="$(echo "${validators_json}" | jq --arg MONIKER "${moniker}" '.[] | select(.description.moniker==$MONIKER)')"
    valoper="$(echo "${validator_json}" | jq -j .operator_address)"
    consensus="$(echo "${validator_json}" | jq -j .consensus_pubkey)"
    jailed="$(echo "${validator_json}" | jq -j .jailed)"
    tokens="$(echo "${validator_json}" | jq -j .tokens)"
    delegator_shares="$(echo "${validator_json}" | jq -j .delegator_shares)"

  fi
  echo "chain       : ${CHAIN_ID}"
  echo "healthcheck : ${healthcheck}"
  echo "image       : $(docker image list "${DOCKER_TAG}" --format "{{.ID}}")"
  echo "container   : $(docker ps -f name="^${DOCKER_TAG}$" --format "{{.ID}}")"
  if [[ ${healthcheck} == healthy ]]; then
  echo "key0_address: $(echo "${keys_json}" | jq -j '.[0].address')"
  echo "valoper     : ${valoper}"
  echo "consensus   : ${consensus}"
  echo "moniker     : ${moniker}"
  echo "catchup     : ${catchup}"
  echo "height      : ${latest_block_height}"
  echo "latest_block: ${seconds_ago} seconds ago"
  echo "jailed      : ${jailed}"
  echo "tokens      : ${tokens}" | cut -f1 -d'.'
  echo "dlg_shares  : ${delegator_shares}" | cut -f1 -d'.'
  echo "voting_power: ${voting_power}" | cut -f1 -d'.'
  fi
  echo "usage_mb    : $(du -sm "${DATA_DIR}" 2>/dev/null | tr '\t' ' ' | tr -s '[:space:]')"
  priv_json_vars
  echo "priv_json   : ${priv_json_active}" 
}

priv_json_switch() {
  # shellcheck source=/dev/null
  source "${PROJECT_DIR}/${NETWORK_DIR}/${NETWORK}.env"
  PRIV_JSON_LIST=",$(find "${DATA_DIR}/identity" -type f -name '*.json' -exec sh -c 'filename=$(basename -- "$1") && printf "${filename%.*},"' _ {} \;)"
  priv_json_vars
  if [[ $# -eq 0 ]]; then
    basename -- "${priv_json_active}" .json
    exit 0
  fi
  case $1 in
    list)
      echo "${PRIV_JSON_LIST}" | tr ',' '\n' | grep -v '^$'
      exit 0
    ;;
    save)
      shift
      if [[ $# -gt 1 ]]; then usage; exit 1; fi
      if [[ "${priv_json_active}" != "${priv_json_destination}" ]]; then echo "ERR identical file - ${priv_json_active}"; exit 1; fi
      if [[ $# -eq 0 ]]; then echo -n "identity name: "; read -r save_as; else save_as="$1"; fi
      if [ ! -f "${priv_json_destination}" ]; then echo "ERR file not found - ${priv_json_destination}"; exit 1; fi
      cp "${priv_json_destination}" "${DATA_DIR}/identity/${save_as}.json"
      echo "identity saved to ${DATA_DIR}/identity/${save_as}.json"
      exit 0
    ;;
    remove)
      shift
      if [[ $# -ne 1 ]]; then usage; exit 1; fi
      file="${DATA_DIR}/identity/$1.json"
      if [ -f "${file}" ]; then
        rm -f "${file}"
        echo "removed ${file}"
        exit 0
      else
        echo "ERR file not found - ${file}"
        exit 1 
      fi
    ;;
    rename)
      shift
      if [[ $# -ne 2 ]]; then usage; exit 1; fi
      file="${DATA_DIR}/identity/$1.json"
      dest="${DATA_DIR}/identity/$2.json"
      if [ -f "${file}" ]; then
        mv "${file}" "${dest}"
        echo "renamed ${file} to ${dest}"
        exit 0
      else
        echo "ERR file not found - ${file}"
        exit 1 
      fi
    ;;
    scp)
      shift
      if [[ $# -ne 2 ]]; then usage; exit 1; fi
      file="${DATA_DIR}/identity/$1.json"
      if [ -f "${file}" ]; then
        scp "${file}" "$2:${file}"
        echo "copied ${file} to $2:${file}"
        exit 0
      else
        echo "ERR file not found - ${file}"
        exit 1 
      fi
    ;;
    *) if [[ $# -ne 1 ]]; then usage; exit 1; fi ;;
  esac
  priv_json_source="${DATA_DIR}/identity/$1.json"
  if ! [[ ${PRIV_JSON_LIST} =~ ,$1, ]]; then
    echo "ERR file not found - ${priv_json_source}"
    exit 1
  fi
  if [ -f "${priv_json_destination}" ]; then
    if diff -q "${priv_json_source}" "${priv_json_destination}" >/dev/null 2>&1; then
      echo "source and destination have the same content, doing nothing..."
      exit 0
    fi
    cat << EOF
----------------- !!!WARNING!!! -----------------
you're about to replace the ${PRIV_JSON} file
source: ${DATA_DIR}/identity/${priv_json_source}
dest  : ${DATA_DIR}/server/config/${PRIV_JSON}
make sure you don't perform double signing
EOF
    echo -n "are you sure? (yes/no): "; read -r user_input
    if [[ ${user_input} != yes ]]; then
      echo "aborted"
      exit 0
    fi
  fi
  docker_ps="$(docker ps -f name="${DOCKER_TAG}" | grep -c "${DOCKER_TAG}" || true)"
  if [[ ${docker_ps} -ne 0 ]]; then docker_stop; restart="true"; fi
  cp "${priv_json_source}" "${priv_json_destination}"
  priv_json_vars
  echo "identity changed to $(basename -- "${priv_json_active}" .json)"
  if [[ ${restart:-false} == true ]]; then docker_start; fi
}

rsync_data() {
  # shellcheck source=/dev/null
  source "${PROJECT_DIR}/${NETWORK_DIR}/${NETWORK}.env"
  cat << EOF
----------------- !!!WARNING!!! -----------------
you're about to copy data to a remote server
the source container will be down for a few seconds
source: ${DATA_DIR}
dest  : $1:${DATA_DIR}
remove: $1:${DATA_DIR}/server/config/priv_*.json
make sure you know what you're doing
EOF
  echo -n "are you sure? (yes/no): "; read -r user_input
  if [[ ${user_input} != yes ]]; then
    echo "aborted"
    exit 0
  fi
  ssh "$1" "mkdir -p \"${DATA_DIR}\"/server/config"
  docker_ps="$(docker ps -f name="${DOCKER_TAG}" | grep -c "${DOCKER_TAG}" || true)"
  if [[ ${docker_ps} -ne 0 ]]; then
    echo "container is running, live rsync in progress..."
    rsync -azhe ssh "${DATA_DIR}/" "${1}:${DATA_DIR}/" --delete || if [[ $? != "24" ]]; then exit 1; fi
    docker_stop
    restart="true"
  fi
  echo "container is stopped, rsync remaining data in progress..."
  rsync -azhe ssh "${DATA_DIR}/" "${1}:${DATA_DIR}/" --delete
  ssh "$1" "find \"${DATA_DIR}/server/config\" -maxdepth 1 -type f -name 'priv_*.json' -exec rm \"{}\" \\;"
  if [[ ${restart:-false} == true ]]; then docker_start; fi
  echo "data successfuly transfered"
}

create_envprops() {
cat <<EOF > "${ENV_PROPS}"
network=none
EOF
}

setenv() {
  test -f "${ENV_PROPS}" || create_envprops
  if ! [[ ${NETWORKS} =~ ,$1, ]]; then
    echo "ERR network $1 not found in ${NETWORK_DIR} dir"
    exit 1
  fi
  perl -pi -e "s/network=.*/network=$1/;" "${ENV_PROPS}"
  getenv
  # shellcheck source=/dev/null
  source "${PROJECT_DIR}/${NETWORK_DIR}/${NETWORK}.env"
  # if the data directories do not exist when docker starts, they will be created as root
  # we do not want that, so we create them now
  mkdir -p "${DATA_DIR}/server"
  mkdir -p "${DATA_DIR}/cli"
  mkdir -p "${DATA_DIR}/identity"
}

getenv() {
  NETWORK="$(grep '^network=.*$' "${ENV_PROPS}" | cut -d'=' -f2 || true)"
  if [[ ${NETWORK} = "" ]]; then
    echo "ERR ${ENV_PROPS} file is invalid, please set your network first"
    exit 1
  fi
  if ! [[ ${NETWORKS} =~ ,${NETWORK}, ]]; then
    echo "ERR network ${NETWORK} found in ${ENV_PROPS}, but not found in ${NETWORK_DIR} dir"
    exit 1
  fi
}

trace() {
  cmd="$(echo "$*" | tr -s '[:space:]')"
  console_log "INFO running ${cmd}"
  eval "$@"
}

ignore() {
  "$@" || true
}

console_log() {
  ts="$(date -u "+%Y-%m-%d %H:%M:%S%z (%Z)") ${0##*/}"
  echo "${ts} $*"
}

trap_parser() {
  bash_cmd="${BASH_COMMAND}"
  exp_arr=()
  for arg in $bash_cmd; do
    [ "${arg:0:1}" == '$' ] && arg="$(echo "${arg:1}" | tr -d '{}')" && arg=${!arg}
    exp_arr+=("${arg}")
  done
  exp_cmd="${exp_arr[*]}"
  case "${exp_cmd}" in
    # do nothing
    'return '*|'read '*)
    ;;
    # everything else
    *)
      console_log "ERR exit code ${1} running ${exp_cmd}"
      exit "${1}"
    ;;
  esac
}

main "$@"

} # this ensures the entire script is downloaded #
