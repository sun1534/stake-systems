# benchmarking on existing ubuntu server

``` bash
cd "${HOME}"
curl https://sh.rustup.rs -sSf | sh
source "$HOME/.cargo/env"
git clone https://github.com/solana-labs/solana.git
cd solana
TAG="$(git describe --tags $(git rev-list --tags --max-count=1))"
git checkout $TAG
sudo apt -y install libudev-dev libssl-dev pkg-config clang libclang-dev llvm
cargo build --release
./fetch-perf-libs.sh
NDEBUG=1 ./multinode-demo/setup.sh

# stop solana
sudo systemctl stop solana

# Open three separate sessions for the next phase

# Session 1
cd "${HOME}/solana"
NDEBUG=1 ./multinode-demo/faucet.sh --cap 2000000000000000

# Session 2
cd "${HOME}/solana"
NDEBUG=1 ./multinode-demo/bootstrap-validator.sh --gossip-port 10000

# Session 3 - here you'll get your results
cd "${HOME}/solana"
NDEBUG=1 ./multinode-demo/bench-tps.sh --duration 180 --thread-batch-sleep-ms 0 --entrypoint 127.0.0.1:10000

# start solana
rm -fr "${HOME}/validator-ledger"
sudo systemctl start solana
```
