#!/usr/bin/env bash
set -exo pipefail

# oh-my-zsh environment
# sudo apt -y install zsh curl git vim
rm -fr "${HOME}/.oh-my-zsh"
rm -f "${HOME}/.zshrc"
rm -f "${HOME}/.zprofile"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
perl -pi -e 's/^ZSH_THEME=.*$/ZSH_THEME=\"stasys\"/;' "${HOME}/.zshrc"
perl -pi -e 's/^plugins=.*$/plugins=(git kubectl docker z last-working-dir history)/;' "${HOME}/.zshrc"

cat << "EOF" >> "${HOME}/.oh-my-zsh/themes/stasys.zsh-theme"
PROMPT='$USER@%{$fg[white]%}%m:%{$fg[cyan]%}%(3~|.../%2~|%~) %{$fg_bold[green]%}%# %{$reset_color%}'
ZSH_THEME_GIT_PROMPT_PREFIX=""
ZSH_THEME_GIT_PROMPT_SUFFIX=""
ZSH_THEME_GIT_PROMPT_DIRTY="*"
ZSH_THEME_GIT_PROMPT_CLEAN="^"
EOF

cat << "EOF" >> "${HOME}/.zshrc"
if [[ "${SSH_DIR}" == "" ]]; then readonly SSH_DIR="${HOME}/.ssh"; fi
if [[ "${SSH_AGENT_CONFIG}" == "" ]]; then readonly SSH_AGENT_CONFIG="${SSH_DIR}/agent"; fi
if [ ! -d "${SSH_DIR}" ]; then mkdir "${SSH_DIR}"; fi
ps -eo command,user | grep "ssh-agent \+$(whoami)" > /dev/null || ssh-agent > "${SSH_AGENT_CONFIG}"
chmod 600 "${SSH_AGENT_CONFIG}"
eval "$(cat "${SSH_AGENT_CONFIG}")" >/dev/null
LOOKUP_DIRS=("${HOME}/.ssh")
IDENTITY='*id_*'
for i in "${LOOKUP_DIRS[@]}"; do
  for j in $(find "${i}" -name ${IDENTITY} -not -path "${i}/*.pub" -type f -print); do
    ssh-add "${j}" 2>/dev/null
    pubkey="$(ssh-keygen -y -f "${j}")"
    echo "${j} ${pubkey}"
  done
done
EOF