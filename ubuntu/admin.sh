#!/usr/bin/env bash
set -eEuo pipefail

# make sure we are root
if [[ "$(whoami)" != "root" ]]; then
  echo "This script must run as root!"
  exit 1
fi

# adding admin user
if ! getent passwd "${ADMIN_USER}" > /dev/null; then
  useradd -ms /bin/bash "${ADMIN_USER}"
  usermod -aG sudo "${ADMIN_USER}"
fi

# admin user uses sudo without password prompt
echo "${ADMIN_USER} ALL=(ALL) NOPASSWD:ALL" | tee "/etc/sudoers.d/${ADMIN_USER}-no-passwd"
