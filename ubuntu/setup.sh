#!/usr/bin/env bash
set -eEuo pipefail

# make sure we are root
if [[ "$(whoami)" != "root" ]]; then
  echo "This script must run as root!"
  exit 1
fi

# install prerequisites
# apt-get update && apt install -y curl
# upgrade packages - optional, but highly reccomended
# apt-get -y upgrade --autoremove

# define our repo
export STASYS_REPO="https://gitlab.com/alexandruast/stake-systems"

# define admin user - this can be the default ubuntu user
export ADMIN_USER="user"

# run root setup script
curl -s "${STASYS_REPO}/-/raw/master/ubuntu/admin.sh" | bash -E

# run user setup script
sudo -i -u "${ADMIN_USER}" bash -c "export STASYS_REPO='${STASYS_REPO}' && curl -s '${STASYS_REPO}/-/raw/master/ubuntu/user.sh' | bash -E"

# install docker
sudo -i -u "${ADMIN_USER}" bash -c "curl -s '${STASYS_REPO}/-/raw/master/ubuntu/docker.sh' | bash -E"

# change passwords - optional, but highly recommended
# sh -c "set -x; echo '${ADMIN_USER}:$(openssl rand -base64 32)' | chpasswd"
# sh -c "set -x; echo 'root:$(openssl rand -base64 32)' | chpasswd"
sudo su - "${ADMIN_USER}"
