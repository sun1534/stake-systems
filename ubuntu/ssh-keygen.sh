#!/usr/bin/env bash
set -eEuo pipefail

# create admin user ssh key and add it to authorized_keys
mkdir -p "${HOME}/.ssh"
[[ ! -f "${HOME}/.ssh/id_ed25519" ]] && ssh-keygen -o -a 100 -t ed25519 -C '' -N '' -f "${HOME}/.ssh/id_ed25519"
[[ ! -f "${HOME}/.ssh/authorized_keys" ]] && < "${HOME}/.ssh/id_ed25519.pub" tee "${HOME}/.ssh/authorized_keys"
chmod 600 "${HOME}/.ssh/id_ed25519"
chmod 600 "${HOME}/.ssh/authorized_keys"
